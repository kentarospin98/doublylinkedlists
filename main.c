#include <stdio.h>
#include <stdlib.h>

// A Single Node in a doubly linked list
struct dnode {
	struct dnode* next;	// Pointer to the next node.
	int data; 		// Data. Stores a single int
	struct dnode* prev;	// Pointer to the previous node.
};

// Doubly Linked List
struct dlist {
	struct dnode* head;	// Pointer to the first node in the list
	struct dnode* tail;	// Pointer to the last node in the list
};

struct dlist* startlist(int d);					// Creates a doubly linked list with one node storing the int d and returns the pointer
int addtostart(struct dlist* listptr, int d);			// Addes a node storing the int d to the start of thelist at address dlist
int addtoend(struct dlist* listptr, int d);			// Addes a node storing the int d to the end of the list at address dlist
int deletenode(struct dlist* listptr, struct dnode* node);	// Deletes a node at the address node
int deletelist(struct dlist* listptr);				// Deletes a list and all nodes in it
int printlist(struct dlist* listptr);				// Prints the contents of a list

int main() {
	struct dlist* listptr = startlist(0);		// Creates a new list with the digit 0 and stores the address in listptr
	printf("Enter '?' for help\n");
	while(0 == 0) {
		char input;
		int d;
		scanf("%c", &input);
		switch (input) { 			// Interactive Menu to call various functions
			case 't':
				printf("Add a digit to the tail of the list: ");
				scanf("%d", &d);
				addtoend(listptr, d);
				break;
			case 'h':
				printf("Add a digit to the head of the list: ");
				scanf("%d", &d);
				addtostart(listptr, d);
				break;
			case 'p':
				printf("Printing the list\n");
				printlist(listptr);
				break;
			case 'd':
				printf("Deleting the tail node\n");
				deletenode(listptr, listptr->tail);
				break;
			case 'D':
				printf("Deleting the head node\n");
				deletenode(listptr, listptr->head);
				break;
			case 'q':
				deletelist(listptr);
				return 0;
				break;
			case '?':
				printf("Instructions\n");
				printf("'t' : Add a digit to the tail\n");
				printf("'h' : Add a digit to the head\n");
				printf("'p' : Print out the list\n");
				printf("'d' : Delete the tail\n");
				printf("'D' : Delete the head\n");
				printf("'q' : Quit\n");
				printf("'?' : Help\n");
		}
	}
	deletelist(listptr);
	return 0;
}

struct dlist* startlist(int d) {
	struct dlist* list;
	struct dnode* node;
	list = calloc(1, sizeof(*list)); 	// Allocates memory for a new list
	node = calloc(1, sizeof(*node));	// Allocates memory for a new node
	node->data = d;				// Stores the digit d in the new node
	node->next = node->prev = NULL;		// Sets the next and prev pointers of the new node to null
	list->head = list->tail = node;		// Stores the pointer to the node in the head and tail fields of the list
	return list;				// Returns the pointer to the list
}

int addtostart(struct dlist* list, int d) {
	struct dnode* newnode;
	newnode = calloc(1, sizeof(*newnode));	// Allocates memory for a new node
	newnode->data = d;			// Stores the digit d in the new node
	newnode->prev = NULL;			// Sets the prev pointer of the new node to NULL
	newnode->next = list->head;		// Stores the pointer to the old first node in the prev field of the new first node
	if(list->head != NULL) {		// If the list head is not NULL (not empty)
		list->head->prev = newnode;	// Stores the pointer to the new first node in the prev field of the old first node
	} else {
		list->tail = newnode;		// Set the tail as the new node
	}
	list->head = newnode;			// Stores the pointer to the new node in the head field of the list
	return 0;
}

int addtoend(struct dlist* list, int d) {
	struct dnode* newnode;
	newnode = calloc(1, sizeof(*newnode));	// Allocates memory for a new node
	newnode->data = d;			// Stores the digit d in the new node
	newnode->next = NULL;			// Sets the next pointer of the new node to NULL
	newnode->prev = list->tail;		// Stores the pointer to the old last node in the prev field of the new last node
	if(list->tail != NULL) {		// If the list tail is not NULL (not empty)
		list->tail->next = newnode;	// Stores the pointer to the new last node in the next field of the old last node
	} else {
		list->head = newnode;		// Sets the head as the new node
	}
	list->tail = newnode;			// Stores the pointer to the new node in the tail field of the list
	return 0;
}

int deletenode(struct dlist* list, struct dnode* node) {
	if(node->prev != NULL) {		// If a previous node exists
		node->prev->next = node->next;	// Points the previous Node's next field to the node in the next field of the current node
	}
	if(node->next != NULL){			// If a next node exists
		node->next->prev = node->prev;	// Points the Next Node's prev field to the prev field of the current node
	}
	if(list->head == node) {		// If the Current node is the head of the list
		list->head = node->next;	// Then set the next node to be the new head
	}
	if(list->tail == node) {		// If the Current node is the tail of the list
		list->tail = node->prev;	// Then set the previous node to be the new tail
	}
	free(node);				// Deallocate the memory taken by the current node
	return 0;
}

int deletelist(struct dlist* list) {
	struct dnode* currentnode = list->head;		// Starts at the head of the list
	struct dnode* nextnode;
	while(0 == 0){					// Runs a loop forever
		if(currentnode != NULL){		// If the current node exists
			nextnode = currentnode->next;	// Then save the next node in the nextnode 
			free(currentnode);		// Deallocate the memory taken by the current node
			currentnode = nextnode;		// Move on to the next node
		} else {
			break;				// If the current node does not exist then break out of the loop
		}
	}
	free(list);					// Deallocate the memory taken by the list
	return 0;
}

int printlist(struct dlist* list) {
	struct dnode* currentnode = list->head;		// Sets the current node to the first node in the list
	while(currentnode != NULL) {			// Runs a loop forever
		printf("%d\n", currentnode->data);	// Prints the data in the Current node
		currentnode = currentnode->next;	// Else set the next node as the current node
	}
	return 0;
}
